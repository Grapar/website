---
title: "Neuigkeiten und Veranstaltungen der Berliner-Plattform:"
draft: false
---

### Bitland trifft Baumland

*Sonntag, 23. Februar 2020 um 11:00 Uhr in der [alten Gießerei](https://gies.se/)*

Dieses neue Format soll es ermöglichen, die Bits mit den Bäumen zu vernetzen, herauszufinden, wo Unterstützung gebraucht wird und wie eure Vorstellungen einer nachhaltigen Digitalisierung aussehen. Wir werden dort kurz uns als Bits &Bäume-Plattform und die im Rahmen der Konferenz formulierten elf Forderungen zu einer nachhaltigen Digitalisierung vorstellen, um dann gemeinsam ins Gespräch zu kommen. Die Bandbreite der Themen darf dabei politische Fragen (z. B. Was bedeutet es, Digitalisierung demokratisch zu gestalten?) genauso umfassen wie konkrete technische Anliegen (z. B. Welche Open-Source-Alternativen gibt es zu gängigen Cloudanbietern?).

Wichtig ist, dass wir Erfahrungen austauschen und ein Gefühl dafür bekommen, welche Veranstaltungen, Gäste und Workshops in Zukunft nützlich sein könnten.

Aktuelles zu dieser und anderen Veranstaltung & weiteren Themen immer -> [im Forum](https://discourse.bits-und-baeume.org/t/2020-02-23-bitland-trifft-baumland-11-00-uhr-alte-giesserei-berlin/296)
