---
title: "footer"
draft: false
---

#### Über uns:

Bits & Bäume Plattform Berlin ist eine Initiative vom AStA TU Berlin Wissenschafts- und Technikkritikreferat in Kooperation mit dem Baumhaus-Projektraum. Wir organisieren eine lokale Plattform für Austausch & Zusammenarbeit von unten für eine nachhaltige Digitalisierung in Berlin.

Das B&B-Berlin-Kern-Team besteht aktuell aus Karen Wohlert, Patrick Luzina, Mathias Renner und Marius Ferdin
