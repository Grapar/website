# Website Bits & Bäume Berlin

This repository stores the raw content of https://berlin.bits-und-baeume.org


# How to edit this website

## Prepare
- Clone this repository
- [Install hugo](https://gohugo.io/getting-started/installing/)
- Inside the cloned repository, run `hugo server -w`
- You should see the website at `localhost:1313/website/`

## Perform changes
- Now change the content of the website: First checkout a new branch (master branch is write-protected) with `git checkout -B <choose a descriptive name of your edit>`
- Perform your edits and check the local hugo server how your changes look like.
- If you are happy, git add, commit and push your changes to the new branch (the command line will help you with the correct commands)

# Notes
- Gitlabs internal Website is: `https://bub-berlin.gitlab.io/website/`

