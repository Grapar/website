---
title: "Impressum"
layout: 'single'
url: '/impressum'
draft: false
---

## Impressum

### Verantwortlich und Inhalt / Gestaltung und Konzeption:

- Verein zur Förderung einer nachhaltigen urbanen Kultur (VFnuK e.V.)
- c/o Max Martens, Gerichtstr. 23, 13347 Berlin
- Telefon: +49 176 78566593
- E-Mail: vfnuk@mailbox.org
- Gemeinschaftlich vertretungsberechtigt: Max Martens, Maria Marggraf



## Datenschutzerklärung

### I. Name und Anschrift des Verantwortlichen

Der Verantwortliche im Sinne der Datenschutz-Grundverordnung und anderer
nationaler Datenschutzgesetze der Mitgliedsstaaten sowie sonstiger
datenschutzrechtlicher Bestimmungen:

Patrick Luzina, Mainzerstraße 15, 10247 Berlin, Deutschland

### II. Name und Anschrift des Datenschutzbeauftragten

Der Datenschutzbeauftragte des Verantwortlichen ist:

Patrick Luzina, Mainzerstraße 15, 10247 Berlin, Deutschland


### III. Allgemeines zur Datenverarbeitung

1. Umfang der Verarbeitung personenbezogener Daten

Wir verarbeiten personenbezogene Daten unserer Nutzer grundsätzlich nur,
soweit dies zur Bereitstellung einer funktionsfähigen Website sowie
unserer Inhalte und Leistungen erforderlich ist. Die Verarbeitung
personenbezogener Daten unserer Nutzer erfolgt regelmäßig nur nach
Einwilligung des Nutzers. Eine Ausnahme gilt in solchen Fällen, in denen
eine vorherige Einholung einer Einwilligung aus tatsächlichen Gründen
nicht möglich ist und die Verarbeitung der Daten durch gesetzliche
Vorschriften gestattet ist.

2. Rechtsgrundlage für die Verarbeitung personenbezogener Daten

Soweit wir für Verarbeitungsvorgänge personenbezogener Daten eine
Einwilligung der betroffenen Person einholen, dient Art. 6 Abs. 1 lit. a
EU-Datenschutzgrundverordnung (DSGVO) als Rechtsgrundlage.  Bei der
Verarbeitung von personenbezogenen Daten, die zur Erfüllung eines
Vertrages, dessen Vertragspartei die betroffene Person ist, erforderlich
ist, dient Art. 6 Abs. 1 lit. b DSGVO als Rechtsgrundlage. Dies gilt
auch für Verarbeitungsvorgänge, die zur Durchführung vorvertraglicher
Maßnahmen erforderlich sind. Soweit eine Verarbeitung personenbezogener
Daten zur Erfüllung einer rechtlichen Verpflichtung erforderlich ist,
der unser Unternehmen unterliegt, dient Art. 6 Abs. 1 lit. c DSGVO als
Rechtsgrundlage.

Für den Fall, dass lebenswichtige Interessen der betroffenen Person oder
einer anderen natürlichen Person eine Verarbeitung personenbezogener
Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO als
Rechtsgrundlage.

Ist die Verarbeitung zur Wahrung eines berechtigten Interesses unseres
Unternehmens oder eines Dritten erforderlich und überwiegen die
Interessen, Grundrechte und Grundfreiheiten des Betroffenen das
erstgenannte Interesse nicht, so dient Art. 6 Abs. 1 lit. f DSGVO als
Rechtsgrundlage für die Verarbeitung.

3. Datenlöschung und Speicherdauer

Die personenbezogenen Daten der betroffenen Person werden gelöscht oder
gesperrt, sobald der Zweck der Speicherung entfällt. Eine Speicherung
kann darüber hinaus erfolgen, wenn dies durch den europäischen oder
nationalen Gesetzgeber in unionsrechtlichen Verordnungen, Gesetzen oder
sonstigen Vorschriften, denen der Verantwortliche unterliegt, vorgesehen
wurde. Eine Sperrung oder Löschung der Daten erfolgt auch dann, wenn
eine durch die genannten Normen vorgeschriebene Speicherfrist abläuft,
es sei denn, dass eine Erforderlichkeit zur weiteren Speicherung der
Daten für einen Vertragsabschluss oder eine Vertragserfüllung besteht.

### IV. Bereitstellung der Website und Erstellung von Logfiles

1. Beschreibung und Umfang der Datenverarbeitung

Bei jedem Aufruf unserer Internetseite erfasst unser System
automatisiert Daten und Informationen vom Computersystem des aufrufenden
Rechners.

Folgende Daten werden hierbei erhoben:

(1) Informationen über den Browsertyp und die verwendete Version
(2) Das Betriebssystem des Nutzers
(3) Den Internet-Service-Provider des Nutzers
(4) Die IP-Adresse des Nutzers
(5) Datum und Uhrzeit des Zugriffs
(6) Websites, von denen das System des Nutzers auf unsere Internetseite
    gelangt
(7) Websites, die vom System des Nutzers über unsere Website aufgerufen
    werden

Die Daten werden ebenfalls in den Logfiles unseres Systems gespeichert.
Nicht hiervon betroffen sind die IP-Adressen des Nutzers oder andere
Daten, die die Zuordnung der Daten zu einem Nutzer ermöglichen. Eine
Speicherung dieser Daten zusammen mit anderen personenbezogenen Daten
des Nutzers findet nicht statt.

2. Rechtsgrundlage für die Datenverarbeitung

Rechtsgrundlage für die vorübergehende Speicherung der Daten ist Art. 6
Abs. 1 lit. f DSGVO.

3. Zweck der Datenverarbeitung

Die vorübergehende Speicherung der IP-Adresse durch das System ist
notwendig, um eine Auslieferung der Website an den Rechner des Nutzers
zu ermöglichen. Hierfür muss die IP-Adresse des Nutzers für die Dauer
der Sitzung gespeichert bleiben.

4. Dauer der Speicherung

Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes
ihrer Erhebung nicht mehr erforderlich sind. Im Falle der Erfassung der
Daten zur Bereitstellung der Website ist dies der Fall, wenn die
jeweilige Sitzung beendet ist.

5. Widerspruchs- und Beseitigungsmöglichkeit

Die Erfassung der Daten zur Bereitstellung der Website und die
Speicherung der Daten in Logfiles ist für den Betrieb der Internetseite
zwingend erforderlich. Es besteht folglich seitens des Nutzers keine
Widerspruchsmöglichkeit.

In diesen Zwecken liegt auch unser berechtigtes Interesse an der
Datenverarbeitung nach Art. 6 Abs. 1 lit. f DSGVO.

### V. Rechte der betroffenen Person

6. Recht auf Beschwerde bei einer Aufsichtsbehörde

Unbeschadet eines anderweitigen verwaltungsrechtlichen oder
gerichtlichen Rechtsbehelfs steht Ihnen das Recht auf Beschwerde bei
einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres
Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen
Verstoßes, zu, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie
betreffenden personenbezogenen Daten gegen die DSGVO verstößt.

Die Aufsichtsbehörde, bei der die Beschwerde eingereicht wurde,
unterrichtet den Beschwerdeführer über den Stand und die Ergebnisse der
Beschwerde einschließlich der Möglichkeit eines gerichtlichen
Rechtsbehelfs nach Art. 78 DSGVO.
