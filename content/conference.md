---
title: "Rückblick B&B Konferenz"
draft: false
---

Im November 2018 fand in Berlin mit fast 2.000 Teilnehmer:innen die bislang größte Konferenz zu Digitalisierung & Nachhaltigkeit statt: die ["Bits & Bäume"](https://bits-und-baeume.org).

Neben jeder Menge Vernetzungsleistung, vielen hochinteressanten [Audio- und Videoaufzeichnungen](https://media.ccc.de/c/bub2018) von Vorträgen und Diskussionen sind die [11 Forderungen der Bits & Bäume Konferenz 2018](https://bits-und-baeume.org/forderungen/info/de) das wesentliche Ergebnis dieser Konferenz. [Fotogalerie](https://cloud.bits-und-baeume.org/index.php/apps/gallery/s/eTH4H8zwdeWmjkE), [PDF-Programm](https://bits-und-baeume.org/downloads/bits-und-baeume-2018-programmheft.pdf)


### Bits & Bäume wächst zur Bewegung!

Im Mai 2019 hat das Orga-Team der Konferenz der Bits & Bäume-Idee in die Freiheit entlassen: Es wird dazu [aufgerufen](https://bits-und-baeume.org/waechst/de) lokal eigenen Treffen und Veranstaltungen zu Digitalisierung und Nachhaltigkeit realisieren.
